#!/bin/bash
# Run this in your enviorment to setup all the libraries needed

# Update pip
python -m pip install --upgrade pip

# Install all dependencies
function pip_install {
  for p in $@; do
    sudo pip install $p
    if [ $? -ne 0 ]; then
      echo "could not install $p - abort"
      exit 1
    fi
  done
}

pip install pyaudio
pip install wave
pip install sounddevice
pip install soundfile
pip install google-cloud-speech
pip install numpy
