# Speech to Text Experiment
*Developed By: Ethan Hindes*

This repo is a one-night project I decided to try out. After one of my friends
told me that they were planning on looking into the google speech-to-text API
for a fun presentation, having the API write to the console while he is talking. 
I decided that I had to give it a shot out of boredom and curiosity during this quarantine! 
Here are the results!

## References
- [Reading A List of Files from a Directory](https://thispointer.com/python-how-to-get-list-of-files-in-directory-and-sub-directories/)
- [Recording Upon Sound Detected](https://stackoverflow.com/questions/18406570/python-record-audio-on-detected-sound)
- [Playing and Recording Sound](https://realpython.com/playing-and-recording-sound-python/)
- [Google Cloud API Example](https://cloud.google.com/speech-to-text/docs/sync-recognize)

## Conclusion
Overall, the Google Cloud is a blast to use! I hope to further develop with these
tools in the feature and create many fun applications. I may use this code to 
create a search by voice feature in an application I am developing for one of 
my classes. This entire repo took me an hour or two to create... Needless to say,
the API's Google provides are very powerful and I would recommend them any day 
of the week.